"""
Utilities here aim to provide a quick relief to the pain of encountering something, like an institute code, that is no
longer in use but has some present-day successor.
"""

from icms_orm.cmspeople import Institute, Person


class SupersedersResolver(object):

    __model = {
        (Institute.code, 'TBILISI-IHEPI'): 'TBILISI-TSU',
        (Person.instCode, 'TBILISI-IHEPI'): 'TBILISI-TSU',
        (Institute.code, 'MOSCOW-RDIPE'): 'MOSCOW-NIKIET',
        (Person.instCode, 'MOSCOW-RDIPE'): 'MOSCOW-NIKIET',
        (Institute.code, 'MEXICO-IPN'): 'MEXICO-CINVESTAV',
        (Person.instCode, 'MEXICO-IPN'): 'MEXICO-CINVESTAV',
        (Institute.code, 'SOFIA-CLMI'): 'SOFIA-ISER',
        (Person.instCode, 'SOFIA-CLMI'): 'SOFIA-ISER',
        (Institute.code, 'SUEZ_CANAL'): 'ASRT-ENHEP',
        (Person.instCode, 'SUEZ_CANAL'): 'ASRT-ENHEP',
        (Institute.code, 'TIFR-EHEP'): 'TIFR',
        (Institute.code, 'BOSTON'): 'BOSTON-UNIV',
        (Institute.code, 'NOTRE-DAME'): 'NOTRE_DAME',
        (Institute.code, 'INR'): 'MOSCOW-INR',
        (Institute.code, 'IHEP'): 'PROTVINO',
        # renamed on 13-11-2019
        (Institute.code, 'ISHAFAN-UNIV'): 'ISFAHAN-IUT',
        (Person.instCode, 'ISHAFAN-UNIV'): 'ISHAFAN-IUT',
    }

    @classmethod
    def get_present_value(cls, ia, value):
        value = isinstance(value, str) and value.strip() or value
        return cls.__model.get((ia, value), value)


