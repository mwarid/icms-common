from icms_orm.cmspeople.people import Person, PeopleFlagsAssociation
from icmsutils.businesslogic.flags import Flag
from icmsutils.prehistory import PersonAndPrehistoryWriter


def suspend_epr(db_session, object_person, actor_person):
    writer = PersonAndPrehistoryWriter(db_session=db_session, object_person=object_person, actor_person=actor_person)
    if object_person.get(Person.isAuthorSuspended) is not True:
        writer.set_new_value(Person.isAuthorSuspended, True)
    writer.apply_changes(do_commit=True)


def unsuspend_epr(db_session, object_person, actor_person):
    writer = PersonAndPrehistoryWriter(db_session=db_session, object_person=object_person, actor_person=actor_person)
    if object_person.get(Person.isAuthorSuspended) is True:
        writer.set_new_value(Person.isAuthorSuspended, False)
    writer.apply_changes(do_commit=True)


def cbi_set_author_yes(db_session, author_to_be, cbi):
    """
    Called upon completing an application successfully, grants signing rights
    """
    writer = PersonAndPrehistoryWriter(db_session=db_session, object_person=author_to_be, actor_person=cbi)
    if author_to_be.get(Person.isAuthorAllowed) is not True:
        writer.set_new_value(Person.isAuthorAllowed, True)
    if author_to_be.get(Person.isAuthorBlock) is True:
        writer.set_new_value(Person.isAuthorBlock, False)
    writer.revoke_flag(Flag.MISC_AUTHORNO)
    writer.apply_changes(do_commit=True)


def cbi_set_author_no(db_session, author_to_be_not, cbi):
    """
    Called upon completing an application successfully, defers signing rights
    """
    writer = PersonAndPrehistoryWriter(db_session=db_session, object_person=author_to_be_not, actor_person=cbi)
    if author_to_be_not.get(Person.isAuthorAllowed) is not False:
        writer.set_new_value(Person.isAuthorAllowed, False)
    if author_to_be_not.get(Person.isAuthorBlock) is True:
        writer.set_new_value(Person.isAuthorBlock, False)
    writer.grant_flag(Flag.MISC_AUTHORNO)
    writer.apply_changes(do_commit=True)