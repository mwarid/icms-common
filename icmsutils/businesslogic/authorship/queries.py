from icmsutils.prehistory.prehistory_model import PrehistoryModel


def get_primary_authorship_eligible_capacity_names():
    return {'Doctoral Student', 'Physicist'}


def get_secondary_authorship_eligible_capacity_names():
    return {'Engineer', 'Engineer Electronics', 'Engineer Mechanical', 'Engineer Software'}


def determine_authorship_application_start_date(cms_id, person=None, history=None, reraise=False, prehistory_model=False):
    """
    Tons of arguments for backwards-compatibility
    :param cms_id:
    :param person:
    :param history:
    :param prehistory_model:
    :param reraise: when True, exceptions will not be trapped so that they can be analysed by the enclosing code
    :return:
    """
    if not prehistory_model:
        prehistory_model = PrehistoryModel(cms_id=cms_id, person=person, history=history)
    assert isinstance(prehistory_model, PrehistoryModel), 'prehistory_model not an instance of PrehistoryModel!'
    return prehistory_model.get_authorship_application_start_date()



