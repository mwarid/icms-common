from icmsutils.exceptions import IcmsException


class RequestProcessingException(IcmsException):
    pass


class UnsupportedRequestTypeException(RequestProcessingException):
    def __init__(self, requested_type):
        _msg = 'Request type {type} has not been fully registered (requires providing a proper ' \
               'processor and executor classes)'.format(type=type)
        super(UnsupportedRequestTypeException, self).__init__(_msg)


class IllegalRequestProcessorClassException(RequestProcessingException):
    def __init__(self, class_):
        _msg = 'Provided {class_}'.format(class_=class_)
        super(IllegalRequestProcessorClassException, self).__init__(_msg)


class IllegalRequestExecutorClassException(RequestProcessingException):
    def __init__(self, class_):
        _msg = 'Provided {class_} '.format(class_=class_)
        super(IllegalRequestExecutorClassException, self).__init__(_msg)


class InsufficientPriviligesException(RequestProcessingException):
    def __init__(self, actor_data, request, action):
        request_str = (request.id and 'id: %d, ' %
                       request.id or '') + ('type %s' % request.type)
        _msg = 'User [CMS ID {cms_id}] cannot perform action "{action}" on this request [{request_str}]'.format(
            cms_id=actor_data.cms_id, action=action, request_str=request_str
        )
        super(InsufficientPriviligesException, self).__init__(_msg)


__all__ = ['RequestProcessingException', 'UnsupportedRequestTypeException', 'IllegalRequestExecutorClassException',
           'IllegalRequestProcessorClassException', 'InsufficientPriviligesException']
