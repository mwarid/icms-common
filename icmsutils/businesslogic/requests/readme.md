# Requests Processing module

## General requirements
- Client apps need to be able to register their own request types along with corresponding Processors and Executors
- Need the ability to fetch all the Requests pending approval (as well as quickly distinguish completed from pending)
  - Also the ability to quickly fetch requests pending approval of a particular person
    - However, we don't want to tie a request to a CMS ID or a Flag
- Need a mechanism of checking if request of given type can be created by given person
- Every action on a given request shall trigger a corresponding processor
  - Processor called upon action need to create subsequent steps or perform the action initially pending the approval
    - Processors store all the logic of creating approval steps store
    - Processors can store the logic of performing the requested action or
    - Executors can store only that logic
      - Request.type and Request.data sufficient to instantiate an Executor and run it once the approval path is cleared

## Request actions

Actions will be performed through a centralised singleton-like object, the `RequestsService`.
`RequestsService` shall offer the following interface:
- `reqister_executor_class(request_type, executor_class)`
- `register_processor_class(request_type, processor_class)`
- `create_request(type, actor_data, **request_params)` - actor_data being a new wrapper for that kind of info
- `approve_step(step_id, actor_data)` - in cases where multiple steps exist that can be approved by the same user
- `approve_next_step(request_id, actor_data)` - in simple cases where we just need to pass the ball on
- `reject_step(step_id, actor_data)`
- `reject_next_step(request_id, actor_data)`
- `can_take_step(step_id, actor_data)`
- `can_act_upon_request(request_id, actor_data)`
- `get_pending_steps(actor_data)`
- `get_pending_requests(actor_data)`


Will need some exceptions too, like:
- `CannotCreateRequestException`
- `InsufficientRequestDataException`
- `CannotApproveRequestException`
- `CannotExecuteRequestException`

## DB Models

Request
- id
- creator_cms_id
- creation_date
- processing data (JSON to instantiate the executor)
- type (free text, sorry - so that client apps can store their request types)
- status (storing here to avoid going through all related steps)

ApprovalStep
- id
- chain_id
- created_by [cms_id of the user whose action led to creating of this step]
- created_on
- handled_by
- handled_on
- label
- status
- deadline

### Processing
Processing will be performed by `RequestProcessor` objects.
Whenever `RequestManager` receives a call to act upon a certain request, it determines the type and instantiates
the corresponding Processor to delegate the work to.

Whenever the request processing advances (ie. a step, including creation, is processed) some number of next steps can be created by the processor and persisted in the database. They can be created one by one as approval proceeds or more can be created in advance - this is up to the processor's implementation. `RequestManager`'s methods `approve_step` and `reject_step` offer the necessary interface. As long as the request has only one step pending, supplying the `step_id` is not necessary in that context.

Once a workflow is complete, the executor is launched (unless a deferred execution date is specified.)


#### Handling app-specific Request types
An app (EPR, Tools, Notes) can register its own request types along with the corresponding processors and executors.
Ideally by subclassing the `BaseRequestDefinition`.

## TODO
- no email messages are generated as of yet