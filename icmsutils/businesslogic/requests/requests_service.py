from datetime import date
import logging
import traceback
from typing import Dict, Iterable, List, Optional, Set, Type
import sqlalchemy

from sqlalchemy.orm.query import Query

from icmsutils.businesslogic.requests import exceptions
from icms_orm.common import RequestStatusValues, RequestStepStatusValues, RequestStep, Request
from sqlalchemy.orm import joinedload
from icmsutils import ActorData
from icmsutils.businesslogic.requests.base_classes import BaseRequestProcessor, BaseRequestExecutor


log: logging.Logger = logging.getLogger(__name__)


class RequestsService(object):
    _processor_classes: Dict[str, Type[BaseRequestProcessor]] = {}
    _executor_classes: Dict[str, Type[BaseRequestExecutor]] = {}
    # request as such does not have a separate subclass per type and thus we store the names only
    _request_type_names: Set[str] = set()

    # THE PUBLIC API SECTION

    @classmethod
    def get_registered_request_types(cls) -> Set[str]:
        _supported = set(cls._processor_classes.keys()) & set(
            cls._executor_classes.keys())
        return {_t for _t in cls._request_type_names if _t in _supported}

    @classmethod
    def register_request_type_name_with_processor_and_executor(cls, request_type_name: str, processor_class: Type[BaseRequestProcessor], executor_class: Type[BaseRequestExecutor]):
        """
        :param request_type_name: a string
        :param processor_class:
        :param executor_class:
        :return:
        """
        if request_type_name in cls._request_type_names:
            raise exceptions.RequestProcessingException(
                '%s request type name already registered!' % request_type_name)
        if processor_class is None or processor_class == BaseRequestProcessor or not issubclass(processor_class, BaseRequestProcessor):
            raise exceptions.IllegalRequestProcessorClassException(
                processor_class)
        if executor_class is None or not issubclass(executor_class, BaseRequestExecutor):
            raise exceptions.IllegalRequestExecutorClassException(
                executor_class)

        cls._request_type_names.add(request_type_name)
        cls._processor_classes[request_type_name] = processor_class
        cls._executor_classes[request_type_name] = executor_class

    @classmethod
    def create_request(cls, request_type_name: str, actor_data: ActorData, remarks: Optional[str] = None, execution_date: Optional[date] = None, **request_processing_data) -> Request:
        log.debug('Creating request stub.')
        if request_type_name not in cls.get_registered_request_types():
            raise exceptions.UnsupportedRequestTypeException(
                requested_type=request_type_name)

        assert isinstance(actor_data, ActorData)
        _request: Request = Request.from_ia_dict({
            Request.creator_cms_id: actor_data.cms_id,
            Request.processing_data: request_processing_data,
            Request.type: request_type_name,
            Request.remarks: remarks,
            Request.status: RequestStatusValues.PENDING_APPROVAL,
            Request.scheduled_execution_date: execution_date,
        })
        # No need to forward the remarks as with creation they are stored in the request itself.
        # Request will be committed to DB in _handle_step_action
        cls._handle_step_action(request=_request, action=BaseRequestProcessor.Action.CREATE, actor_data=actor_data,
                                remarks=None)
        return _request

    @classmethod
    def can_act(cls, request_id: Optional[int], actor_data: ActorData, action: Optional[BaseRequestProcessor.Action] = None, step_id: Optional[int] = None) -> bool:
        request: Request = cls.get_request_data(request_id)
        processor: BaseRequestProcessor = cls._get_request_processor_instance(
            request)
        return processor.can_act(actor_data, action, step_id)

    @classmethod
    def approve_step(cls, request_id: Optional[int], actor_data: ActorData, step_id: Optional[int] = None, remarks: Optional[str] = None):
        request: Request = cls.get_request_by_step_id(
            step_id) if step_id else cls.get_request_data(request_id or 0)
        return cls._handle_step_action(request=request, actor_data=actor_data, step_id=step_id, remarks=remarks, action=BaseRequestProcessor.Action.APPROVE)

    @classmethod
    def reject_step(cls, request_id: Optional[int], actor_data: ActorData, step_id: Optional[int] = None, remarks: Optional[str] = None):
        request: Request = cls.get_request_by_step_id(
            step_id) if step_id else cls.get_request_data(request_id or 0)
        return cls._handle_step_action(request=request, actor_data=actor_data, step_id=step_id, remarks=remarks, action=BaseRequestProcessor.Action.REJECT)

    @classmethod
    def get_request_steps_pending_action(cls, actor_data: Optional[ActorData] = None):
        """
        :param actor_data:
        :return: Iterable of RequestStep objects
        """
        _all_pending_steps = Request.session().query(RequestStep).\
            join(Request, RequestStep.request_id == Request.id).\
            filter(Request.status == RequestStatusValues.PENDING_APPROVAL).\
            filter(RequestStep.status == RequestStepStatusValues.PENDING).\
            options(joinedload(RequestStep.request)).all()
        if not actor_data:
            return _all_pending_steps
        else:
            _steps_that_can_be_acted_upon = []
            for _step in _all_pending_steps:
                _p = cls._get_request_processor_instance(
                    request_data=_step.get(RequestStep.request))
                if _p.can_act(actor_data=actor_data, step=_step):
                    _steps_that_can_be_acted_upon.append(_step)
            return _steps_that_can_be_acted_upon

    @classmethod
    def get_request_data(cls, request_id: int) -> Request:
        """
        :param request_id: The DB ID of the request.
        :return: Database Request object along with its corresponding steps
        """
        _ssn = Request.session()
        return _ssn.query(Request).filter(Request.id == request_id).options(joinedload(Request.steps)).one()

    @classmethod
    def get_request_by_step_id(cls, step_id: int) -> Request:
        _ssn = Request.session()
        _sq = Query([RequestStep.request_id.label('sought_id'), RequestStep.id]).filter(
            RequestStep.id == step_id).subquery()
        return _ssn.query(Request).join(_sq, Request.id == _sq.c.sought_id).options(joinedload(Request.steps)).one()

    @classmethod
    def get_requests_by_remarks(cls, remarks: str) -> List[Request]:
        return Request.session().query(Request).filter(Request.remarks.like('%{remarks}%'.format(remarks=remarks))).all()

    @classmethod
    def get_requests_by_type(cls, request_type: str) -> List[Request]:
        return Request.session().query(Request).filter(Request.type == request_type).all()

    @classmethod
    def get_matching_requests(cls, types_pool: Optional[Iterable[str]] = None, statuses_pool: Optional[Iterable[str]] = None) -> Iterable[Request]:
        '''
        Returns the requests matching provided criteria.
        '''
        q: Query = Request.session().query(Request)
        if types_pool:
            q = q.filter(sqlalchemy.or_(
                *[Request.type == _t for _t in types_pool]))
        if statuses_pool:
            q = q.filter(sqlalchemy.or_(
                *[Request.status == _s for _s in statuses_pool]))
        return q.all()

    @classmethod
    def get_actionable_by_type(cls, request_type: str, actor_data: ActorData, action: Optional[BaseRequestProcessor.Action] = None, step=None) -> List[Request]:
        data: List[Request] = cls.get_requests_by_type(
            request_type=request_type)
        data = [e for e in data if cls._get_request_processor_instance(
            e).can_act(actor_data, action=action, step=step)]
        return data

    # THE HEAVY LIFTING SECTION OF PRIVATE AND PROTECTED METHODS

    @classmethod
    def _handle_step_action(cls, request: Request, action, actor_data: ActorData, step_id: Optional[int] = None, remarks: Optional[str] = None):
        """
        The core method of request processing, linking it all together.
        Every action, including request creation should pass through this method to assure uniform handling.
        """
        try:
            assert isinstance(actor_data, ActorData)
            _processor = cls._get_request_processor_instance(
                request_data=request)
            assert isinstance(_processor, BaseRequestProcessor)
            _processor.handle_step(
                action=action, actor_data=actor_data, step_id=step_id, remarks=remarks)
            # todo: test that all the associated steps are preserved correctly in the database
            Request.session().add(request)
            try:
                cls._execute_request_if_cleared(request)
            finally:
                Request.session().commit()
        except Exception as e:
            log.error(f'{str(e)} performing {action} on a request!')
            log.error(traceback.format_exc())
            Request.session().rollback()
            raise

    @classmethod
    def _get_request_processor_instance(cls, request_data: Request) -> BaseRequestProcessor:
        _cls: Type[BaseRequestProcessor] = cls._get_request_processor_class(request_data.get(Request.type))
        assert issubclass(_cls, BaseRequestProcessor) and _cls != BaseRequestProcessor
        return _cls(request=request_data)

    @classmethod
    def _get_request_processor_class(cls, request_type_name) -> Type[BaseRequestProcessor]:
        _cls = cls._processor_classes.get(request_type_name)
        assert issubclass(_cls, BaseRequestProcessor)
        return _cls

    @classmethod
    def _get_request_executor_class(cls, request_type_name: str) -> Type[BaseRequestExecutor]:
        """
        :type request_type_name: str
        """
        _cls: Type[BaseRequestExecutor] = cls._executor_classes[request_type_name]
        assert issubclass(_cls, BaseRequestExecutor)
        return _cls

    @classmethod
    def _get_request_executor_instance(cls, request_data) -> BaseRequestExecutor:
        _cls = cls._get_request_executor_class(
            request_type_name=request_data.get(Request.type))
        assert issubclass(_cls, BaseRequestExecutor)
        return _cls(**request_data.get(Request.processing_data))

    @classmethod
    def _is_cleared_for_execution(cls, request_data: Request) -> bool:
        return request_data.get(Request.status) == RequestStatusValues.READY_FOR_EXECUTION

    @classmethod
    def _execute_request_if_cleared(cls, request_data: Request):
        if cls._is_cleared_for_execution(request_data):
            try:
                _executor: BaseRequestExecutor = cls._get_request_executor_instance(
                    request_data)
                _executor.execute()
                request_data.set(Request.status, RequestStatusValues.EXECUTED)
            except Exception as e:
                log.error('Request execution failed for request ID {request_id}'.format(
                    request_id=request_data.get(Request.id)))
                log.error(traceback.format_exc())
                request_data.set(
                    Request.status, RequestStatusValues.FAILED_TO_EXECUTE)
                raise
