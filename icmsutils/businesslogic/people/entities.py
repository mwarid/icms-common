class CmsCountry(object):
    def __init__(self, name):
        self.__data = {'name': name}

    @property
    def name(self) -> str:
        return self.__data.get('name')


class CmsTeam(object):
    def __init__(self, code, name=None, country: CmsCountry = None):
        self.__code = code
        self.__name = name
        self.__country = country

    @property
    def name(self) -> str:
        return self.__name

    @property
    def code(self) -> str:
        return self.code

    @property
    def country(self) -> CmsCountry:
        return self.__country


class CmsUser(object):

    def __init__(self, cms_id: int, user_name: str = None, first_name: str = None, last_name: str = None,
                 team: CmsTeam = None):
        self.__cms_id = cms_id
        self.__user_name = user_name
        self.__first_name = first_name
        self.__last_name = last_name
        self.__team = team

    @property
    def cms_id(self) -> int:
        return self.__cms_id

    @property
    def user_name(self) -> str:
        return self.__user_name

    @property
    def first_name(self) -> str:
        return self.__first_name

    @property
    def last_name(self) -> str:
        return self.__last_name

    @property
    def team(self) -> CmsTeam:
        return self.__team
