from email_templates.email_template import EmailTemplateBase


class EmailTemplateAuthorshipStatusChange(EmailTemplateBase):

    _sender = 'icms-support@cern.ch'
    _recipients = '{recipient_email}'
    _subject = '[iCMS] Author status {earned_or_lost}'
    _cc = '{team_leader_emails}'
    _bcc = 'icms-support@cern.ch'
    _source_app = 'common'
    _body = u'''Dear {recipient_name},

This message has been generated automatically to notify you of a change to your authorship status.
Following the last automated check you are {now_an_author_or_no_longer_an_author}.

Best regards,
iCMS mailing system'''

    def __init__(self, recipient_name, recipient_email, team_leader_emails, bool_has_rights, sender_email=None):
        super(EmailTemplateAuthorshipStatusChange, self).__init__(
            recipient_name=recipient_name,
            recipient_email=recipient_email,
            team_leader_emails=team_leader_emails,
            earned_or_lost=bool_has_rights and 'earned' or 'lost',
            now_an_author_or_no_longer_an_author=bool_has_rights and 'now a CMS Author' or 'no longer a CMS Author'
        )
        if sender_email:
            self._sender = sender_email