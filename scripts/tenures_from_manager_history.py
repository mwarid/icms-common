from datetime import date

from scripts.prescript_setup import db
from icms_orm.common import ManagerHistory
from icms_orm.common import Institute
from icms_orm.epr import Project, Pledge, Task, EprInstitute, CMSActivity
from icms_orm.common import PositionName, OrgUnitTypeName, OrgUnitType, OrgUnit, Position, Tenure, ApplicationAsset, AppNameValues
from sqlalchemy import func
import sqlalchemy as sa
import logging
import re


class AssetHelper(object):
    KEY = 'tenures_from_managers_history'

    @staticmethod
    def get():
        return ApplicationAsset.retrieve(AssetHelper.KEY)

    @staticmethod
    def store(ids):
        return ApplicationAsset.store(AssetHelper.KEY, ids, application=AppNameValues.COMMON)


class TenureDesc(object):
    def __init__(self, position_name, unit_type, unit_domain):
        self.position_name = position_name
        self.unit_type = unit_type
        self.unit_domain = unit_domain

    def __str__(self):
        return '{pos_name} at {domain} {type}'.format(pos_name=self.position_name, domain=self.unit_domain,
                                                      type=self.unit_type)

    @classmethod
    def map_to_position_unit_domain(cls, string, def_position=None, def_unit=None, def_domain=None):
        pass

    @classmethod
    def translate(cls, some_str):
        """
        :param some_str:
        :return:
        """
        return None, None, None

    @classmethod
    def from_role_string(cls, role_str):
        role_str = role_str.strip()
        # first the very specific matches
        for ref_str, pos_name, unit_type_name, domain in [
            ('SP', PositionName.SPOKESPERSON, OrgUnitTypeName.OFFICE, 'Spokesperson'),
            ('SP-Deputy', PositionName.DEPUTY, OrgUnitTypeName.OFFICE, 'Spokesperson'),
            ('CB', PositionName.CHAIRPERSON, OrgUnitTypeName.BOARD, 'Collaboration'),
            ('CB-Deputy', PositionName.DEPUTY, OrgUnitTypeName.BOARD, 'Collaboration'),
            ('CB-Secretary', PositionName.SECRETARY, OrgUnitTypeName.BOARD, 'Collaboration'),
            ('Physics-Deputy', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Physics'),
            ('Computing', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'Computing'),
            ('Computing-Deputy', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Computing'),
            ('L1-Pub comm', PositionName.CHAIRPERSON, OrgUnitTypeName.COMMITTEE, 'Publications'),
            ('L1-Tech', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'Technical'),
            ('L1-DTech', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Technical'),
            ('L1-Computing', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'Computing'),
            ('L1-DComputing', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Computing'),
            ('L1-PUB comm', PositionName.CHAIRPERSON, OrgUnitTypeName.COMMITTEE, 'Publications'),
            ('L1-Phys', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'Physics'),
            ('L1-DPhys', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Physics'),
            ('L1-DRun', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'Run'),
            ('L1-Run', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'Run'),
            ('PPD', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA, 'PPD'),
            ('PPD-Deputy', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA, 'PPD'),
            ('Run-Coordination-WBM', PositionName.MANAGER, OrgUnitTypeName.GENERIC_UNIT_LEVEL_2, 'WBM')
        ]:
            if role_str.lower() == ref_str.lower():
                return cls(position_name=pos_name, unit_type=unit_type_name, unit_domain=domain)

        # then some regular ones
        for pattern, pos_name, unit_type_name in [
            (r'(.+)-Coordination$', PositionName.COORDINATOR, OrgUnitTypeName.COORDINATION_AREA),
            (r'(.+) Coordinator Deputy$', PositionName.DEPUTY, OrgUnitTypeName.COORDINATION_AREA),
            (r'(.+)-Committee-Deputy$', PositionName.DEPUTY, OrgUnitTypeName.COMMITTEE),
            (r'(.+)-Committee$', PositionName.CHAIRPERSON, OrgUnitTypeName.COMMITTEE),
            (r'PAG \| (\w+)', PositionName.CONVENER, OrgUnitTypeName.PAG),
            (r'POG \| (\w+)', PositionName.CONVENER, OrgUnitTypeName.POG),
            (r'^(?!L1)(.+)-Deputy', PositionName.DEPUTY, OrgUnitTypeName.SUBDETECTOR),
            (r'^(?!L1)(.+)', PositionName.MANAGER, OrgUnitTypeName.SUBDETECTOR),
            (r'^L1-D(.+)', PositionName.DEPUTY, OrgUnitTypeName.SUBDETECTOR),
            (r'^L1-(.+)-Deputy', PositionName.DEPUTY, OrgUnitTypeName.SUBDETECTOR),
            (r'^L1-(.+)', PositionName.MANAGER, OrgUnitTypeName.SUBDETECTOR)
        ]:
            m = re.match(pattern, role_str)
            if m:
                # if unit_type_name is None and pos_name is None:
                #     pos_name, unit_type_name, unit_domain = cls.guess(m.group(1))
                return cls(position_name=pos_name, unit_type=unit_type_name, unit_domain=m.group(1))
        return None


def main():

    if AssetHelper.get() is not None:
        print('It seems like this import has already been performed, aborting.')
        return

    roles = db.session.query(ManagerHistory.role.distinct()).all()
    matched_roles = {}

    for role in [_r[0] for _r in roles]:
        role = role.strip()
        if role in matched_roles:
            logging.warning('Role {0} already in!'.format(role))
        desc_object = None
        desc_obj = TenureDesc.from_role_string(role)
        matched_roles[role] = desc_obj
    print('Successfully matched {0} out of {1} role descriptions.'.format(len([_x for _x in matched_roles.values() if _x]), len(matched_roles)))

    for _role_str, _desc in matched_roles.items():
        assert isinstance(_desc, TenureDesc)
        print('{0:<30} --- {1}'.format(_role_str, _desc))

    # iterate and check if the unit type is already there
    for _role_str, _desc in matched_roles.items():
        _count = db.session.query(OrgUnitType).filter(OrgUnitType.name == _desc.unit_type).count()
        if _count == 1:
            pass
        elif _count == 0:
            print('Found {0} matching org unit types for {1}'.format(_count, _desc.unit_type))
            _new = None
            for _val in OrgUnitTypeName.values():
                if _val.lower() == _desc.unit_type.lower():
                    _new = OrgUnitType.from_ia_dict({
                        OrgUnitType.name: _val,
                        OrgUnitType.level: {
                                OrgUnitTypeName.POG: 2,
                                OrgUnitTypeName.PAG: 2,
                                OrgUnitTypeName.GENERIC_UNIT_LEVEL_2: 2,
                                OrgUnitTypeName.GENERIC_UNIT_LEVEL_3: 3
                            }.get(_val)})
                    break
            if _new is not None:
                db.session.add(_new)
                db.session.commit()
                print('Successfully added new OrgUnitType: {0}'.format(_val))
            else:
                raise Exception('Could not find appropriate OUTN for {0}'.format(_desc.unit_type))
        else:
            raise Exception('Found too many matches for {0} unit type: {1}'.format(_count, _desc.unit_type))
    # iterate and check if unit of proper type and necessary domain is already there:
    for _role_str, _desc in matched_roles.items():
        _count = db.session.query(OrgUnit).join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).\
            filter(sa.and_(OrgUnitType.name == _desc.unit_type, OrgUnit.domain.ilike(_desc.unit_domain))).count()
        if _count == 1:
            pass
        elif _count == 0:
            print('No unit (yet) of type {0} and domain {1}'.format(_desc.unit_type, _desc.unit_domain))
            _type_id = db.session.query(OrgUnitType.id).filter(OrgUnitType.name == _desc.unit_type).one()
            _new = OrgUnit.from_ia_dict({
                OrgUnit.domain: _desc.unit_domain,
                OrgUnit.type_id: _type_id,
            })
            db.session.add(_new)
            db.session.commit()
            print('Now we have successfully added an OrgUnit of type {0} and domain {1}'.format(_desc.unit_type, _desc.unit_domain))
        else:
            raise Exception('That is too many matches for unit type {0} and domain {1}: {2}'.format(_desc.unit_type, _desc.unit_domain, _count))

    # iterate and check if position of proper name and unit type id is there
    for _role_str, _desc in matched_roles.items():
        assert isinstance(_desc, TenureDesc)
        _count = db.session.query(Position).join(OrgUnitType, Position.unit_type_id == OrgUnitType.id).\
            filter(Position.name == _desc.position_name).filter(OrgUnitType.name == _desc.unit_type).count()
        if _count == 1:
            pass
        elif _count == 0:
            print('No position found: {0} at {1}'.format(_desc.position_name, _desc.unit_type))
            _unit_type_id = db.session.query(OrgUnitType.id).filter(OrgUnitType.name == _desc.unit_type).one()
            _new = Position.from_ia_dict({
                Position.name: _desc.position_name,
                Position.unit_type_id: _unit_type_id
            })
            db.session.add(_new)
            db.session.commit()
        else:
            raise Exception('Too many positions {0} at {1}: {2}'.format(_desc.position_name, _desc.unit_type, _count))

    # now we can add all the tenures. would make sense to merge them though...

    def find_contiguous_ranges(_arr, step_val=1):
        ranges = []
        _start = _arr[0]
        for idx, el in enumerate(_arr):
            if idx > 0:
                if el - _arr[idx - 1] != step_val:
                    ranges.append((_start, _arr[idx-1]))
                    _start = el
        ranges.append((_start, _arr[-1]))
        return ranges


    _new_tenures = []

    _group_cols = [ManagerHistory.cms_id, ManagerHistory.role]
    _data = db.session.query(*(_group_cols + [sa.func.array_agg(ManagerHistory.year)])).group_by(*_group_cols).all()
    for _cms_id, _role, _years in _data:
        _desc = TenureDesc.from_role_string(_role)
        _years = sorted(_years)
        _ranges = find_contiguous_ranges(_years, 1)
        print('CMS ID {0} performed the role of {1} in years {2} - or ranges: {3}'.format(_cms_id, _role, _years, _ranges))

        _unit_id = db.session.query(OrgUnit.id).join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).\
            filter(sa.and_(OrgUnitType.name == _desc.unit_type, OrgUnit.domain.ilike(_desc.unit_domain))).one()
        _position_id = db.session.query(Position.id).join(OrgUnitType, Position.unit_type_id == OrgUnitType.id).\
            filter(sa.and_(Position.name == _desc.position_name, OrgUnitType.name == _desc.unit_type)).one()

        for _range in _ranges:
            _new = Tenure.from_ia_dict({
                Tenure.cms_id: _cms_id,
                Tenure.start_date: date(_range[0], 1, 1),
                Tenure.end_date: date(_range[1], 12, 31),
                Tenure.position_id: _position_id,
                Tenure.unit_id: _unit_id,
            })
        _new_tenures.append(_new)
        db.session.add(_new)

    db.session.commit()
    _ids = [_t.get(Tenure.id) for _t in _new_tenures]
    AssetHelper.store(_ids)


if __name__ == '__main__':
    main()


def that_is_something_irrelevant(year=2018, proj=-1):
    authorsSq = db.session.query()

    subQuery = (db.session.query(
        func.json_build_object(Project.id,
                               func.json_array_length(
                                   func.json_build_array(
                                       func.json_array_length(func.json_agg(Pledge.userId.distinct())),
                                       func.json_array_length(func.json_agg(EprInstitute.code.distinct())),
                                       func.json_array_length(func.json_agg(Institute.country_code.distinct())),
                                   )
                               )
                               ).label('unit')
    )
                .join(CMSActivity, CMSActivity.projectId == Project.id)
                .join(Task, Task.activityId == CMSActivity.id)
                .join(Pledge, Pledge.taskId == Task.id)
                .join(EprInstitute, Pledge.instId == EprInstitute.id)
                .join(Institute, EprInstitute.code == Institute.code)
                .filter(Pledge.year == year)
                .filter(Pledge.status != 'rejected')
                .group_by(Project.id).subquery()
                )
    subQuery = (proj != -1) and subQuery.filter(Project.id == proj) or subQuery
    result = db.session.query(func.array_agg(subQuery.c.unit)).all()
    print('Good news:')
    print(result)
