"""
The methods below for synchronising individual entity types are provided for backwards-compatibility and
to serve as testing shorthands. They will probably be phased out shortly.
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.sync.agents import CountriesSyncAgent
from scripts.sync.agents import PeopleSyncAgent
from scripts.sync.agents import FinalMoSyncAgent
from scripts.sync.agents import AffiliationsSyncAgent
from scripts.sync.agents import DeputyTeamLeadersSyncAgent
from scripts.sync.agents import FundingAgenciesSyncAgent
from scripts.sync.agents import InstitutesSyncAgent
from scripts.sync.agents import TeamLeadersSyncAgent
from scripts.sync import launch as launch


def sync_people():
    launch(agent_classes=[PeopleSyncAgent])


def sync_institutes():
    launch(agent_classes=[InstitutesSyncAgent])


def sync_leaders():
    launch(agent_classes=[TeamLeadersSyncAgent])


def sync_deputies():
    launch(agent_classes=[DeputyTeamLeadersSyncAgent])


def sync_team_leaders():
    sync_leaders()
    sync_deputies()


def sync_affiliations():
    launch(agent_classes=[AffiliationsSyncAgent])


def sync_mo():
    launch(agent_classes=[FinalMoSyncAgent])


def sync_countries():
    """
    The DB will just be populated with all the possibilities
    """
    launch(agent_classes=[CountriesSyncAgent])


def sync_funding_agencies():
    launch(agent_classes=[FundingAgenciesSyncAgent])


def resolve_country(country_name):
    """
    :param country_name:
    :return: alpha_2 official country code
    """
    return CountriesSyncAgent.resolve_country(country_name)


def sync_all():
    # warning: this is NOT the MAIN method of this script (sticks around for some compatibility reasons)
    sync_institutes()
    sync_people()
    sync_affiliations()
    sync_team_leaders()
    sync_countries()
    sync_funding_agencies()
    sync_mo()


if __name__ == '__main__':
    launch()
