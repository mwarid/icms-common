import logging

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import Person as SrcPerson, User as SrcUser, MemberActivity as SrcActivity
from icms_orm.cmspeople import PersonHistory as SrcHistory
from icms_orm.common import InstituteLeader, Affiliation
from icms_orm.common import Person as DstPerson, PersonStatus as DstPersonStatus
from icms_orm.common import PrehistoryTimeline
from sqlalchemy import case

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from .sync_agents_common import BaseSyncAgent





