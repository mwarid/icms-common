from scripts.blocked_engineers.classes import BlockedEngineerIssue

if __name__ == '__main__':
    issues_by_nominal_start_year = {}
    issues_by_outcome = {}
    issues = BlockedEngineerIssue.find_issues()
    for issue in issues:
        assert isinstance(issue, BlockedEngineerIssue)
        print(issue.__unicode__())

        if issue.last_unsuspension:
            print(' --- last unsuspended: {0}'.format(issue.last_unsuspension))
        else:
            print(' --- last registered: {0}'.format(issue.last_registration))
        if issue.has_any_mo:
            print(' --- has MO this year: PHD: {phd} / FREE: {free} / WISH: {wish}'.format(phd=issue.phd_mo, wish=issue.wish_mo, free=issue.free_mo))
        print(' --- total EPR contribution: %.2f' % issue.epr_done)
        print(' --- total EPR dues: %.2f' % issue.epr_dues)
        print(' --- timed out? {0}'.format(issue.timed_out))
        print(' --- outcome: %s' % issue.verdict)

        _nominal_start = issue.nominal_app_start.year
        issues_by_nominal_start_year[_nominal_start] = issues_by_nominal_start_year.get(_nominal_start, [])
        issues_by_nominal_start_year[_nominal_start].append(issue)

        _outcome = issue.verdict
        issues_by_outcome[_outcome] = issues_by_outcome.get(_outcome, [])
        issues_by_outcome[_outcome].append(issue)

    print('There is a total of {0} issues...'.format(len(issues)))

    print(' --- Issues by Year of app start --- ')
    for _year in sorted(issues_by_nominal_start_year.keys()):
        print('{year}: {support}'.format(year=_year, support=len(issues_by_nominal_start_year[_year])))

    print(' --- Issues by proposed solution --- ')
    for key in issues_by_outcome.keys():
        print('{solution}: {support}'.format(solution=key, support=len(issues_by_outcome[key])))
        print(', '.join([str(_i.cms_id) for _i in issues_by_outcome.get(key)]))

        for _issue in issues_by_outcome.get(key):
            assert isinstance(_issue, BlockedEngineerIssue)
            if 'assign due' in key:
                _issue.apply_dues()
            elif 'suspend' in key:
                _issue.apply_suspension()
            elif 'unblock' in key:
                _issue.apply_unblocking()
            elif 'unsure' in key:
                try:
                    _issue.apply_dues()
                    print('Applied dues for CMS ID %d' % _issue.cms_id)
                except Exception as e:
                    print('Did not work for %d' % _issue.cms_id)
