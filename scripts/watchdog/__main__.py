from scripts.prescript_setup import db, config, ConfigProxy
from scripts.watchdog.units import run_watchdogs
import logging

if __name__ == '__main__':
    logging.info('Watchdog classes registered to run: {0}'.format(ConfigProxy.get_watchdog_classes_to_run()))
    run_watchdogs()
