import sqlalchemy as sa
from datetime import date, timedelta, datetime
from icms_orm.epr import TimeLineUser as TLU
import icms_orm

from scripts.prescript_setup import ConfigProxy
from scripts.watchdog.units.watchdog_base import WatchdogBase


class EprWatchdogMixin(object):
    def ssn(self):
        ssn = TLU.session()
        ssn.bind = ssn.manager.get_engine_for_bind_key(icms_orm.epr_bind_key())
        return ssn

class TimeLinesWatchdog(WatchdogBase, EprWatchdogMixin):

    def check(self):
        year_now = date.today().year
        # check if there are any negative year fractions for present year
        neg_count = self.ssn().query(TLU.id).filter(TLU.yearFraction < 0).filter(TLU.year == year_now).count()

        if neg_count:
            self.add_issue('Found %d time-lines with negative year fraction' % neg_count)

        sq = self.ssn().query(TLU.cmsId.label('cms_id'), sa.func.sum(TLU.dueApplicant).label('due_app'),
                sa.func.sum(TLU.dueAuthor).label('due_auth')).filter(TLU.year == year_now).\
                group_by(TLU.cmsId).subquery()

        # check sums of app due
        app_sum_rows = self.ssn().query(sq.c.cms_id, sq.c.due_app).filter(sq.c.due_app > 6.001).all()
        for cms_id, app_due_sum in app_sum_rows:
            self.add_issue('CMS ID %d has %.2f of applicant due in %d' % (cms_id, app_due_sum, year_now))

        # check sums of author due
        auth_sum_row = self.ssn().query(sq.c.cms_id, sq.c.due_auth).filter(sq.c.due_auth > 4.001).all()
        for cms_id, auth_due_sum in auth_sum_row:
            self.add_issue('CMS ID %d has %.2f author due in %d' % (cms_id, auth_due_sum, year_now))

        # check how many pairs of timelines start in the same moment
        sq2 = self.ssn().query(TLU.cmsId.label('cms_id'), TLU.timestamp.label('timestamp'),
                                  sa.func.count(TLU.id).label('total')).group_by(TLU.timestamp, TLU.cmsId).filter(
            TLU.year == date.today().year).subquery()

        for cms_id, timestamp, total in self.ssn().query(sq2.c.cms_id, sq2.c.timestamp, sq2.c.total).filter(
                sq2.c.total > 1).all():
            self.add_issue(
                'CMS ID %d has %d parallel timelines starting at %s. [will attempt to fix]' % (cms_id, total, timestamp))
            self.__fix_parallel_timelines(cms_id, timestamp)

    def __fix_parallel_timelines(self, cms_id, timestamp):
        tlus = self.ssn().query(TLU).filter(TLU.cmsId == cms_id).filter(TLU.timestamp == timestamp).order_by(
            TLU.id).all()
        if len(tlus) == 2:
            assert tlus[0].id < tlus[1].id
            if tlus[0].get(TLU.yearFraction) < 0:
                # first time-line had been "confused" for the most recent one and participated in a split,
                # need to recompute some values
                old_yf = tlus[1].get(TLU.yearFraction)
                new_yf = old_yf + tlus[0].get(TLU.yearFraction)
                scaling_f = new_yf / old_yf
                for attr in [TLU.dueAuthor, TLU.dueApplicant]:
                    tlus[1].set(attr, tlus[1].get(attr) * scaling_f)
                # re-adjust year fractions
                tlus[0].set(TLU.yearFraction, 0)
                tlus[1].set(TLU.yearFraction, new_yf)
                self.add_issue('Fixing parallel time-lines for %d with re-computing the dues.' % cms_id)
            else:
                self.add_issue('Fixing parallel time-lines for %d without re-computing the dues.' % cms_id)
            # regardless of whether the numbers have been messed up or not,
            # make sure that the more recent time-line appears so too in DB
            tlus[1].set(TLU.timestamp, tlus[1].get(TLU.timestamp) + timedelta(seconds=1))
            self.ssn().add(tlus[0])
            self.ssn().add(tlus[1])
            self.ssn().commit()


class LastTimeLinesSyncWatchdog(WatchdogBase, EprWatchdogMixin):
    """
    Checks to see if the EPR sync isn't lagging behind
    """
    def check(self):
        last_stamp = self.ssn().query(sa.func.max(TLU.timestamp)).one()[0]
        if (datetime.now() - last_stamp).days > ConfigProxy.get_watchdog_epr_sync_grace_period():
            self.add_issue('Last EPR time line was created on %s - perhaps there is an issue with sync.' % last_stamp)
        elif last_stamp > datetime.now():
            self.add_issue('Last EPR time line was created on %s - which seems to be in the future.' % last_stamp)