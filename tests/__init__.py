#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import pkgutil
import scripts
import os

from scripts import authorship_applications
from scripts import blocked_engineers
from scripts.cleanup import new_year_exmembers
#from scripts import watchdog
from scripts import auto_admit_pending_authors
from scripts import auto_suspend_failed_applicants
from scripts import compactify_logs
from scripts import determine_authors
from scripts import generate_new_authors_notifications
from scripts import generate_new_authors_reminders
from scripts import ldap_sync
# from scripts import mark_closed_papers_in_db
from scripts import populate_org_unit_tables
# not sure if the following breaks anything:
# from scripts import prehistory
from scripts import pull_cadi_cds_info
from scripts import send_mails
# importing this one makes everything crash
# from scripts import stale_cadilines
from scripts import sync_epr_timelines
from scripts import sync_to_new_db
