from scripts.prescript_setup import db
from icmsutils.businesslogic.flags import Flag
from icmsutils.businesslogic.requests import RequestsService, PersonStatusChangeRequestDefinition
from icmsutils.datatypes import ActorData
import pytest
from icmsutils.icmstest import ddl
from icmsutils.icmstest import assertables
from icmsutils.icmstest.mock.new_icms_mock_factories import MockNewPersonFactory as PersonFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockInstituteFactory as InstFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockInstituteLeaderFactory as LeaderFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockAffiliationFactory as AffiliationFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockLegacyFlagFactory as LegacyFlagFactory
from icms_orm.common import Person, Affiliation, InstituteLeader, Institute, Request, RequestStatusValues, RequestStep
from icms_orm.common import LegacyFlag, RequestStepStatusValues
from icms_orm.cmspeople import Person as OldPerson, Institute as OldInst, MemberStatusValues as MSV
from icmsutils.icmstest.mock.old_icms_mock_factories import MockPersonFactory as OldPersonFactory
from icmsutils.icmstest.mock.old_icms_mock_factories import MockInstFactory as OldInstFactory


@pytest.fixture(scope='class')
def setup_stage():
    ddl.recreate_people_db()
    ddl.recreate_common_db()
    PersonFactory.reset_overrides()

    s = db.session()

    AffiliationFactory.set_overrides(Affiliation, {Affiliation.is_primary: True})

    s.add(InstFactory.create_instance({Institute.code: 'ATI', Institute.name: 'A Test Institute'}))
    s.add(InstFactory.create_instance({Institute.code: 'SOI', Institute.name: 'Some Other Institute'}))
    s.flush()
    s.add(PersonFactory.create_instance({Person.cms_id: 1}))
    s.add(PersonFactory.create_instance({Person.cms_id: 2}))
    s.add(PersonFactory.create_instance({Person.cms_id: 3}))
    s.add(PersonFactory.create_instance({Person.cms_id: 4}))
    s.add(PersonFactory.create_instance({Person.cms_id: 5}))
    s.flush()
    s.add(LeaderFactory.create_instance({InstituteLeader.cms_id: 1, InstituteLeader.inst_code: 'ATI'}))
    s.add(LeaderFactory.create_instance({InstituteLeader.cms_id: 5, InstituteLeader.inst_code: 'SOI'}))
    s.flush()
    s.add(AffiliationFactory.create_instance({Affiliation.inst_code: 'ATI', Affiliation.cms_id: 1}))
    s.add(AffiliationFactory.create_instance({Affiliation.inst_code: 'ATI', Affiliation.cms_id: 2}))
    s.add(AffiliationFactory.create_instance({Affiliation.inst_code: 'ATI', Affiliation.cms_id: 3}))
    s.add(AffiliationFactory.create_instance({Affiliation.inst_code: 'ATI', Affiliation.cms_id: 4}))
    s.add(AffiliationFactory.create_instance({Affiliation.inst_code: 'SOI', Affiliation.cms_id: 5}))
    s.add(LegacyFlagFactory.create_instance({LegacyFlag.cms_id: 4, LegacyFlag.flag_code: Flag.ICMS_SECR}))
    s.flush()
    s.add(OldInstFactory.create_instance({OldInst.code: 'ATI'}))
    s.add(OldInstFactory.create_instance({OldInst.code: 'SOI'}))
    s.flush()
    s.add(OldPersonFactory.create_instance({OldPerson.cmsId: 1, OldPerson.instCode: 'ATI', OldPerson.status: MSV.PERSON_STATUS_CMS}))
    s.add(OldPersonFactory.create_instance({OldPerson.cmsId: 2, OldPerson.instCode: 'ATI', OldPerson.status: MSV.PERSON_STATUS_CMS}))
    s.add(OldPersonFactory.create_instance({OldPerson.cmsId: 3, OldPerson.instCode: 'ATI', OldPerson.status: MSV.PERSON_STATUS_CMS}))
    s.add(OldPersonFactory.create_instance({OldPerson.cmsId: 4, OldPerson.instCode: 'ATI', OldPerson.status: MSV.PERSON_STATUS_CMS}))
    s.add(OldPersonFactory.create_instance({OldPerson.cmsId: 5, OldPerson.instCode: 'SOI', OldPerson.status: MSV.PERSON_STATUS_CMS}))
    s.commit()
    PersonStatusChangeRequestDefinition.register()


class Wrap(object):
    @staticmethod
    def create_status_change_request(cms_id, cms_id_by, new_status=MSV.PERSON_STATUS_CMSEXTENDED):
        return RequestsService.create_request(PersonStatusChangeRequestDefinition.get_name(), ActorData(cms_id=cms_id_by), None, None,
                                      cms_id=cms_id, creator_cms_id=cms_id_by, new_status=MSV.PERSON_STATUS_CMSEXTENDED)

    @staticmethod
    def approve_request(request_id, cms_id_by):
        return RequestsService.approve_step(request_id=request_id, actor_data=ActorData(cms_id=cms_id_by), step_id=None)


@pytest.fixture(scope='class')
def create_request(setup_stage):
    return Wrap.create_status_change_request(cms_id=2, cms_id_by=1)


@pytest.fixture(scope='class')
def approve_request(create_request):
    _req = create_request
    assert isinstance(_req, Request)
    return Wrap.approve_request(request_id=_req.id, cms_id_by=4)


class TestPersonStatusChange(object):
    @pytest.mark.parametrize('cms_id_by', [2, 3, 4, 5])
    def test_others_cannot_create_the_request(self, setup_stage, cms_id_by):
        """
        Nobody but the CBI in charge of CMS ID 2's institute should be able to create such a request
        """
        with pytest.raises(Exception):
            Wrap.create_status_change_request(cms_id=2, cms_id_by=cms_id_by)

    def test_cbi_can_create_request(self, create_request):
        """
        Request should now be created with proper parameters
        """
        assert 1 == assertables.count_in_db(
            Request, {Request.type: PersonStatusChangeRequestDefinition.get_name()})
        assertables.assert_attr_val(Request, {Request.type: PersonStatusChangeRequestDefinition.get_name()}, {
                                    Request.status: RequestStatusValues.PENDING_APPROVAL})

    @pytest.mark.parametrize('cms_id_by', [1, 2, 3, 5])
    def test_non_secretariat_cannot_approve_request(self, create_request, cms_id_by):
        """
        No one but the secretariat shall be able to approve these requests
        """
        with pytest.raises(Exception):
            Wrap.approve_request(create_request.id, cms_id_by)

    def test_secretariat_can_approve_request(self, create_request, approve_request):
        """
        The request should now be approved by the CMS ID 4 and executed, leaving the CMS ID 2 with EXTENDED status
        """
        _req_id = create_request.id
        assert 1 == assertables.count_in_db(Request, {Request.type: PersonStatusChangeRequestDefinition.get_name()})
        assertables.assert_attr_val(Request, {Request.id: _req_id}, {Request.status: RequestStatusValues.EXECUTED})
        assertables.assert_attr_val(RequestStep, {RequestStep.request_id: _req_id}, {RequestStep.status: RequestStepStatusValues.APPROVED})
        assertables.assert_attr_val(OldPerson, {OldPerson.cmsId: 2}, {OldPerson.status: MSV.PERSON_STATUS_CMSEXTENDED})
