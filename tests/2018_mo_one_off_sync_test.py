#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.exceptions import FwdSyncException
from scripts.prescript_setup import db, config
from sqlalchemy import and_, or_
import pytest
from icms_orm.cmspeople import Person as P, Institute as I, MoData as MO, InstStatusValues, MemberActivity
from icmsutils.icmstest.assertables import assert_attr_val, count_in_db
from scripts.sync import ForwardSyncManager, FinalMoSyncAgent, CountriesSyncAgent, \
    FundingAgenciesSyncAgent, PeopleSyncAgent, InstitutesSyncAgent
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
from icmsutils.icmstest.mock import MockPersonMoFactory, MockUserFactory, MockInstFactory, MockPersonFactory, MockActivityFactory
from icms_orm.common import Person as DestP, Institute as DestI, MO as DestMo, FundingAgency as FA, MoStatusValues


class Data(object):
    __instance = None

    @staticmethod
    def _fill_mo(mo, year, person, fa):
        _cols = [getattr(MO, k.key.replace('2011', str(year))) for k in [MO.mo2011, MO.freeMo2011, MO.phdMo2011]]
        _mask = (mo.get(MO.cmsId) + year) % 8
        for _idx, _col in enumerate(_cols, 0):
            # some bitwise mumbo-jumbo to generate a different pattern each time
            mo.set(_col,  (_mask & (1 << _idx)) and 'YES' or 'NO')
        if mo.get(_cols[-1]) == 'YES':
            setattr(mo, MO.phdInstCode2011.key.replace('2011', str(year)), person.get(P.instCode))
            setattr(mo, MO.phdFa2011.key.replace('2011', str(year)), fa)
        return mo.get(_cols[-1]) == 'YES'

    def __init__(self, ppl_per_inst=5, sync_year_from=2017, sync_year_to=2019):
        self._ppl_per_institute = ppl_per_inst
        self._activities = {}
        self._insts = {}
        self._people = {}
        self._users = {}
        self._mo = {}
        MockPersonMoFactory.reset_overrides(MO)
        MockPersonFactory.reset_overrides(P)

    @property
    def phd_activity_id(self):
        return {id for id, a in self._activities.items() if a.get(MemberActivity.name) == 'Physicist'}.pop()

    @classmethod
    def cms_ids(cls):
        return sorted(cls.get_instance()._people.keys())

    @classmethod
    def inst_codes(cls):
        return sorted(cls.get_instance()._insts.keys())

    @classmethod
    def get_person(cls, cms_id):
        return cls.get_instance().people.get(cms_id)

    @classmethod
    def get_mo(cls, cms_id):
        return cls.get_instance()._mo.get(cms_id)

    @classmethod
    def get_inst(cls, code):
        return cls.get_instance()._insts.get(code)

    @classmethod
    def get_phd_fa_code(cls, year, cms_id):
        (_mo_col, _phd_mo_col, _free_mo_col, _phd_ic_col, phd_fa_col) = MO.get_columns_for_year(year=year)
        return cls.get_instance().get_mo(cms_id).get(phd_fa_col)

    @classmethod
    def get_phd_inst_code(cls, year, cms_id):
        (_mo_col, _phd_mo_col, _free_mo_col, phd_ic_col, _phd_fa_col) = MO.get_columns_for_year(year=year)
        return cls.get_instance().get_mo(cms_id).get(phd_ic_col)


    @classmethod
    def _get_year_and_cms_id_tuples(cls, mo_flag=False, free_flag=False, phd_flag=False):
        _r = []
        _synonyms = {True: {'YES'}, False: {'NO', None, ''}}
        for _cms_id in cls.get_instance().cms_ids():
            _mo = cls.get_mo(_cms_id)
            for _year in range(2017, 2020):
                mo_col, phd_mo_col, free_mo_col, _phd_ic_col, _phd_fa_col = MO.get_columns_for_year(_year)
                if _mo.get(mo_col) in _synonyms[mo_flag]:
                    if _mo.get(phd_mo_col) in _synonyms[phd_flag]:
                        if _mo.get(free_mo_col) in _synonyms[free_flag]:
                            _r.append((_year, _cms_id))
        return _r

    @classmethod
    def get_year_and_cms_id_tuples_without_mo_at_all(cls):
        return cls._get_year_and_cms_id_tuples(False, False, False)

    @classmethod
    def get_year_and_cms_id_tuples_with_requested_mo_only(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=True, free_flag=False, phd_flag=False)

    @classmethod
    def get_year_and_cms_id_tuples_with_requested_and_free_mo(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=True, free_flag=True, phd_flag=False)

    @classmethod
    def get_year_and_cms_id_tuples_with_requested_and_phd_mo(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=True, free_flag=False, phd_flag=True)

    @classmethod
    def get_year_and_cms_id_tuples_with_only_free_mo(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=False, free_flag=True, phd_flag=False)

    @classmethod
    def get_year_and_cms_id_tuples_with_only_phd_mo(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=False, free_flag=False, phd_flag=True)

    @classmethod
    def get_year_and_cms_id_tuples_with_all_but_requested_mo_flags(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=False, free_flag=True, phd_flag=True)

    @classmethod
    def get_year_and_cms_id_tuples_with_all_mo_flags(cls):
        return cls._get_year_and_cms_id_tuples(mo_flag=True, free_flag=True, phd_flag=True)


    def setup(self):
        # prepare activities
        _ = [self._activities.update({a.get(MemberActivity.id): a}) for a in MockActivityFactory.create_all()]

        # prepare institutes
        for _i_data in [
            {I.code: 'GVA', I.country: 'Switzerland', I.fundingAgency: 'Switzerland 1', I.cmsStatus: InstStatusValues.YES},
            {I.code: 'LSN', I.country: 'Switzerland', I.fundingAgency: 'Switzerland E', I.cmsStatus: InstStatusValues.YES},
            {I.code: 'NYC', I.country: 'USA', I.fundingAgency: 'USA-DOE', I.cmsStatus: InstStatusValues.YES},
            {I.code: 'LSV', I.country: 'USA', I.fundingAgency: 'USA-NSF', I.cmsStatus: InstStatusValues.YES},
            {I.code: 'SNF', I.country: 'USA', I.fundingAgency: 'USA-DOE', I.cmsStatus: InstStatusValues.YES}
        ]:
            _mock = MockInstFactory.create_instance(instance_overrides=_i_data)
            self._insts.update({_mock.get(I.code): _mock})

        # prepare people
        for _inst_code in self.inst_codes():
            for _i in range(0, self._ppl_per_institute):
                p = MockPersonFactory.create_instance(instance_overrides={P.activityId: self.phd_activity_id, P.instCode: _inst_code})
                self._people[p.cmsId] = p
                mo = MockPersonMoFactory.create_instance(person=p)
                for _year in range(2017, 2020):
                    self._fill_mo(mo, year=_year, person=p, fa=self.get_inst(_inst_code).get(I.fundingAgency))
                self._mo[mo.cmsId] = mo
                u = MockUserFactory.create_instance(person=p)
                self._users[u.cmsId] = u

    def populate_db(self):
        # activities
        _ = [db.session.add(a) for a in self._activities.values()]
        # institutes
        _ = [db.session.add(i) for i in self._insts.values()]
        # people
        for _cms_id in self.cms_ids():
            db.session.add(self._people.get(_cms_id))
            db.session.add(self._users.get(_cms_id))
            db.session.add(self._mo.get(_cms_id))

        db.session.commit()

    @classmethod
    def get_instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
            cls.__instance.setup()
        return cls.__instance


@pytest.fixture(scope='class')
def setup():
    recreate_people_db()
    recreate_common_db()
    test_data = Data.get_instance()
    test_data.populate_db()
    # here comes the sync
    ForwardSyncManager.launch_sync(agent_classes_override=[CountriesSyncAgent, FundingAgenciesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent])
    return test_data


@pytest.fixture(scope='class')
def fa_map(setup):
    return {_fa.id: _fa for _fa in db.session.query(FA).all()}


@pytest.fixture(scope='class')
def dest_mo_map(setup):
    _r = {}
    # ATTENTION: multiple results per year and cms_id possible!
    for _mo in db.session.query(DestMo).order_by(DestMo.timestamp).all():
        _key = (_mo.get(DestMo.year), _mo.get(DestMo.cms_id))
        _r[_key] = _r.get(_key, [])
        _r[_key].append(_mo)
    return _r


class TestPostSyncMoStatus(object):
    """
    Groups tests basing on one successful sync having been carried out
    """
    def test_test_data_makes_sense(self, setup):
        for _year in range(2017, 2020):
            (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MO.get_columns_for_year(_year)
            # check that all the combinations for this year are represented
            assert 1 < count_in_db(MO, {phd_mo_col: 'YES', free_mo_col: 'YES', mo_col: 'YES'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'NO', free_mo_col: 'YES', mo_col: 'YES'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'YES', free_mo_col: 'NO', mo_col: 'YES'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'YES', free_mo_col: 'YES', mo_col: 'NO'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'NO', free_mo_col: 'NO', mo_col: 'YES'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'NO', free_mo_col: 'YES', mo_col: 'NO'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'YES', free_mo_col: 'NO', mo_col: 'NO'})
            assert 1 < count_in_db(MO, {phd_mo_col: 'NO', free_mo_col: 'NO', mo_col: 'NO'})
            # make sure that only people with PHD MO have the FA and IC set
            assert_attr_val(MO, {}, {phd_mo_col: 'YES'}, lambdas_list=[lambda q: q.filter(and_(phd_ic_col != None, phd_ic_col!=''))])
            assert_attr_val(MO, {}, {phd_mo_col: 'YES'}, lambdas_list=[lambda q: q.filter(and_(phd_fa_col != None, phd_fa_col!=''))])
            # also, make sure that there is nobody with PHD MO but without the FA or IC
            assert 0 == count_in_db(MO, {phd_mo_col: 'YES'}, lambdas_list=[lambda q: q.filter(or_(phd_fa_col == None, phd_fa_col == '', phd_ic_col == None, phd_ic_col == ''))])

    # should also check if our cms_id sets, generated by the Data object actually correspond to something meaningful
    for _year, _cms_id in Data.get_year_and_cms_id_tuples_with_all_but_requested_mo_flags():
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MO.get_columns_for_year(_year)
        _mo = Data.get_mo(_cms_id)
        assert _mo.get(mo_col) != 'YES'
        assert _mo.get(phd_mo_col) == 'YES'
        assert _mo.get(free_mo_col) == 'YES'

    for _year, _cms_id in Data.get_year_and_cms_id_tuples_with_requested_mo_only():
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MO.get_columns_for_year(_year)
        _mo = Data.get_mo(_cms_id)
        assert _mo.get(mo_col) == 'YES'
        assert _mo.get(free_mo_col) != 'YES'
        assert _mo.get(phd_mo_col) != 'YES'

    for _year, _cms_id in Data.get_year_and_cms_id_tuples_with_requested_and_free_mo():
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MO.get_columns_for_year(_year)
        _mo = Data.get_mo(_cms_id)
        assert _mo.get(mo_col) == 'YES'
        assert _mo.get(free_mo_col) == 'YES'
        assert _mo.get(phd_mo_col) != 'YES'

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_without_mo_at_all())
    def test_no_mo_at_all_syncs_not(self, setup, dest_mo_map, year, cms_id):
        _dest_mo = dest_mo_map.get((year, cms_id), 'no, nothing\'s there')
        assert _dest_mo is 'no, nothing\'s there'

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_only_phd_mo())
    def test_phd_mo_only_sync(self, setup, dest_mo_map, year, cms_id):
        # probably someone got approved and subsequently swapped-out
        _dest_mos = dest_mo_map.get((year, cms_id), [])
        assert 2 == len(_dest_mos)
        _a = _dest_mos[0]
        _b = _dest_mos[1]

        assert MoStatusValues.APPROVED == _a.get(DestMo.status)
        assert MoStatusValues.SWAPPED_OUT == _b.get(DestMo.status)

        _latest = DestMo.query_for_latest(cms_id=cms_id, year=year).one()
        assert _latest == _b

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_only_free_mo())
    def test_free_mo_only_sync(self, setup, dest_mo_map, year, cms_id):
        self.test_requested_and_free_mo_sync(setup, dest_mo_map, year, cms_id)

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_requested_mo_only())
    def test_requested_mo_only_sync(self, setup, dest_mo_map, year, cms_id):
        _dest_mo = dest_mo_map.get((year, cms_id))[0]
        assert _dest_mo is not None
        assert _dest_mo.get(DestMo.status) == MoStatusValues.PROPOSED
        assert _dest_mo.get(DestMo.cms_id) == cms_id
        assert _dest_mo.get(DestMo.year) == year

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_requested_and_phd_mo())
    def test_requested_and_phd_mo_sync(self, setup, fa_map, dest_mo_map, year, cms_id):
        _dest_mo = dest_mo_map.get((year, cms_id))[0]
        assert _dest_mo is not None
        assert _dest_mo.get(DestMo.status) == MoStatusValues.APPROVED
        assert _dest_mo.get(DestMo.cms_id) == cms_id
        assert _dest_mo.get(DestMo.year) == year

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_requested_and_free_mo())
    def test_requested_and_free_mo_sync(self, setup, dest_mo_map, year, cms_id):
        _dest_mos = dest_mo_map.get((year, cms_id))
        assert len(_dest_mos) > 0
        _dest_mo = _dest_mos[0]
        assert _dest_mo is not None
        assert _dest_mo.get(DestMo.status) == MoStatusValues.APPROVED_FREE_GENERIC
        assert _dest_mo.get(DestMo.cms_id) == cms_id
        assert _dest_mo.get(DestMo.year) == year

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_all_but_requested_mo_flags())
    def test_all_but_requested_mo_sync(self, setup, dest_mo_map, year, cms_id):
        self.test_phd_mo_only_sync(setup, dest_mo_map, year, cms_id)

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_all_mo_flags())
    def test_all_mo_flags_sync(self, setup, fa_map, dest_mo_map, year, cms_id):
        self.test_requested_and_phd_mo_sync(setup, fa_map, dest_mo_map, year, cms_id)

    # ensure that those with a free-mo have no FA
    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_requested_and_free_mo())
    def test_free_mo_holders_have_no_fa_or_ic_assigned(self, setup, dest_mo_map, year, cms_id):
        _mo = (dest_mo_map.get((year, cms_id)))[0]
        assert _mo.get(DestMo.fa_id) is None
        assert _mo.get(DestMo.inst_code) is None

    # ensure that those with approved or swapped out M&O have the same FA as in the source data
    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_requested_and_phd_mo())
    def test_approved_mo_holders_have_the_same_fa_and_ic_assigned_across_the_databases(self, setup, fa_map, dest_mo_map, year, cms_id):
        _mo = dest_mo_map.get((year, cms_id))[0]
        _fa = fa_map.get(_mo.get(DestMo.fa_id))
        assert _fa is not None
        assert Data.get_phd_fa_code(year, cms_id) == _fa.get(FA.name)
        assert Data.get_phd_inst_code(year, cms_id) == _mo.get(DestMo.inst_code)

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_only_phd_mo())
    def test_swapped_out_mo_holders_maintain_correct_fa_and_ic_affiliation(self, setup, fa_map, dest_mo_map, year, cms_id):
        _mo_approved, _mo_swapped = dest_mo_map.get((year, cms_id))
        _fa_id = _mo_approved.get(DestMo.fa_id)
        assert _fa_id is not None
        assert _fa_id == _mo_swapped.get(DestMo.fa_id)
        _fa = fa_map.get(_fa_id)
        assert _fa is not None
        assert Data.get_phd_fa_code(year, cms_id) == _fa.get(FA.name)
        assert Data.get_phd_inst_code(year, cms_id) == _mo_approved.get(DestMo.inst_code)
        assert Data.get_phd_inst_code(year, cms_id) == _mo_swapped.get(DestMo.inst_code)

    @pytest.mark.parametrize('year, cms_id', Data.get_year_and_cms_id_tuples_with_all_but_requested_mo_flags())
    def test_swapped_out_phd_and_free_mo_holders_maintain_correct_fa_and_ic_affiliation(self, setup, fa_map, dest_mo_map, year, cms_id):
        self.test_swapped_out_mo_holders_maintain_correct_fa_and_ic_affiliation(setup, fa_map, dest_mo_map, year, cms_id)

    def test_second_consecutive_sync_fails_with_some_exception(self, setup):
        with pytest.raises(FwdSyncException) as _e_info:
            ForwardSyncManager.launch_sync(agent_classes_override=[FinalMoSyncAgent])
